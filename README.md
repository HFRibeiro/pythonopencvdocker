## Docker build
```
docker build . -t pyopencv

docker run -it pyopencv python

docker run -it -v $(pwd):/code pyopencv
--privileged -v /dev/ttyUSB0:/dev/ttyUSB0

--device=/dev/video0

sudo xhost +local:docker \
&& XSOCK=/tmp/.X11-unix \
&& XAUTH=/tmp/.docker.xauth \
&& xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

docker run -it -v $(pwd):/code \
--network host \
--privileged -v /dev/video0:/dev/video0 \
-e DISPLAY=$DISPLAY \
-v $XSOCK:$XSOCK \
-v $XAUTH:$XAUTH \
-e XAUTHORITY=$XAUTH \
pyopencv /bin/bash

```