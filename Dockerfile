FROM debian:buster-slim
LABEL mantainer="Hélder Ribeiro <helder.freitas.ribeiro@gmail.com>"

RUN apt-get update \
    && apt-get install -y \
        build-essential \
        cmake \
        git \
        wget \
        unzip \
        yasm \
        pkg-config \
        libswscale-dev \
        libavcodec-dev \
        libtbb2 \
        libtbb-dev \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libdc1394-22-dev \
        libavformat-dev \
        libpq-dev \
        libgtk2.0-0 \
        libgtk-3-dev \
        x11-xserver-utils \
        libcanberra-gtk3-module \
        qt5-default \
        python3-pip \
    && rm -rf /var/lib/apt/lists/*

RUN python3 --version

RUN pip3 install numpy flask imutils playsound

RUN cd /home \
    && git clone https://gitlab.com/HFRibeiro/meridiancamera.git \
    && cd /home/meridiancamera/DALSADOCKER \
    && ./corinstall all install \
    && pip3 install pygigev